# This program displays step-by-step instructions
# for disassembling an Acme dryer.
# The main function performs the program's main logic.

def main():
    # Display the start-up messsage.
    startup_message()
    input('Press Enter to see step 1.')
    # Display step 1. 
    step1()
    input('Press Enter to see step 2.')
    # Display step 2. 
    step2()
    input('Press Enter to see step 3')
    # Display step 3. 
    step3()
    input('Press Enter to see step 4')
    # Display step  4. 
    step4()

# The startup_message function displays the
# programs inital message on the screen.
def startup_message(wat,foo):
    print('This program tells you how to')
    print('dissasmble and ACME dryer')
    print('There are 4 steps in the process')
    print()

# The step 1 fucntions displays the instructions
# for step 1. 
def step1():
    print('Step 1: Unplug the dryer and')
    print('move it away from the wall')
    print()

# The step 2 funtion displays the instructions
# for step 2 
def step2():
    print('Step 2: Remove the six screw')
    print('from the back panel')
    print()

# The step 3 fucntion displays the instructions
# for step 3
def step3():
    print('Step 3: Remove the back panel')
    print('from the dryer')
    print()

# The step 4 fucntions displays the instructions
# for step 4
def step4():
    print('Step 4: Pull the top of the dryer')
    print('sraight up')
    print()

# Calls the main function to being the program
main()